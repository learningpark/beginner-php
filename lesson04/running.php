<?php

$filename = 'data.number';

// If file does not exist, set n to 0 
if ( ! file_exists($filename) ) {
    $n = 0;
} else {
    // file exists, so get current value
    $n = file_get_contents($filename);
}
echo "Number = $n\n";
$n++; // increment before writing new value
file_put_contents($filename, $n);