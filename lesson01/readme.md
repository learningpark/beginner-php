# Lesson 1

This is a simple hello program that outputs the word hello and a number.
It is a simple example of how to make a program in PHP that can be run on the command line and how it can output some data.