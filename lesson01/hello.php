<?php

// Output Hello and a new line (\n)
echo "Hello\n";
// Make a variable and make its value 0
$n = 0;
// Output it
echo "n = ". $n . "\n";
// Increment the variable
++$n;
// Output it
echo "n = ". $n . "\n";